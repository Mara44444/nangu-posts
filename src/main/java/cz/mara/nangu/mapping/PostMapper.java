package cz.mara.nangu.mapping;

import cz.mara.nangu.dto.PostDto;
import cz.mara.nangu.messaging.PostMessage;
import cz.mara.nangu.model.Post;
import cz.mara.nangu.model.User;
import org.springframework.stereotype.Component;

@Component
public class PostMapper {

    public PostDto toDto(Post post) {
        PostDto postDto = new PostDto();
        postDto.setText(post.getText());
        return postDto;
    }

    public Post fromDto(PostDto postDto, User author) {
        Post post = new Post();
        post.setAuthor(author);
        post.setText(postDto.getText());
        return post;
    }

    public Post fromMessage(PostMessage postMessage, User author) {
        Post post = new Post();
        post.setAuthor(author);
        post.setText(postMessage.getText());
        return post;
    }
}
