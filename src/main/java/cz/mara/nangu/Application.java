package cz.mara.nangu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "cz.mara.nangu")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
