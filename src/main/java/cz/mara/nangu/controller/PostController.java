package cz.mara.nangu.controller;

import cz.mara.nangu.dto.LongDto;
import cz.mara.nangu.dto.PostDto;
import cz.mara.nangu.dto.PostUpdateDto;
import cz.mara.nangu.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "posts")
public class PostController {

    @Autowired
    private PostService postService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public LongDto createPost(@RequestBody PostDto postDto) {
        return new LongDto(postService.createPost(postDto));
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<PostDto> getPosts() {
        return postService.getPost();
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public PostDto getPost(@PathVariable("id") Long postId) {
        return postService.getPost(postId);
    }

    @DeleteMapping(value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deletePost(@PathVariable("id") Long postId) {
        postService.deletePost(postId);
    }

    @PutMapping(value = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updatePost(@PathVariable("id") Long postId, @RequestBody PostUpdateDto updateDto) {
        postService.updatePost(updateDto, postId);
    }
}
