package cz.mara.nangu.controller;

import cz.mara.nangu.dto.LoginDto;
import cz.mara.nangu.dto.RegisterUserDto;
import cz.mara.nangu.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "registration", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void registerUser(@RequestBody @Valid RegisterUserDto registerUserDto) {
        userService.registerUser(registerUserDto);
    }

    @PostMapping(value = "login", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void login(@RequestBody @Valid LoginDto loginDto, HttpServletRequest request) {
        userService.doLogin(request, loginDto);
    }
}
