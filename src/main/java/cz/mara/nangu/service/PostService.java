package cz.mara.nangu.service;

import cz.mara.nangu.dao.PostRepository;
import cz.mara.nangu.dao.UserRepository;
import cz.mara.nangu.dto.PostDto;
import cz.mara.nangu.dto.PostUpdateDto;
import cz.mara.nangu.mapping.PostMapper;
import cz.mara.nangu.messaging.PostMessage;
import cz.mara.nangu.model.Post;
import cz.mara.nangu.model.User;
import cz.mara.nangu.security.SecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PostService {

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PostMapper postMapper;
    @Autowired
    private SecurityManager securityManager;

    @Transactional(readOnly = true)
    public List<PostDto> getPost() {
        return postRepository.findAll().stream()
                .map(postMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public PostDto getPost(Long postId) {
        Optional<Post> post = postRepository.findById(postId);
        if (!post.isPresent()) {
            throw new IllegalArgumentException("post with given id does not exists");
        }
        return postMapper.toDto(post.get());
    }

    public Long createPost(PostDto postDto) {
        User user = securityManager.getCurrentUser();
        Post post = postMapper.fromDto(postDto, user);
        return postRepository.save(post).getId();
    }

    public void createPost(PostMessage postMessage) {
        Optional<User> user = userRepository.findByUsername(postMessage.getUsername());
        if (!user.isPresent()) {
            throw new IllegalArgumentException("User does not exists");
        }
        Post post = postMapper.fromMessage(postMessage, user.get());
        postRepository.save(post);
    }

    public void updatePost(PostUpdateDto updateDto, Long postId) {
        User user = securityManager.getCurrentUser();
        Optional<Post> post = postRepository.findByIdAndAuthor(postId, user);
        if (!post.isPresent()) {
            throw new IllegalArgumentException("User does not have post with given id");
        }
        post.get().setText(updateDto.getText());
    }

    public void deletePost(Long postId) {
        User user = securityManager.getCurrentUser();
        boolean exists = postRepository.existsByIdAndAuthor(postId, user);
        if (!exists) {
            throw new SecurityException("User does not have posts with given id");
        }
        postRepository.deleteById(postId);
    }

}
