package cz.mara.nangu.service;

import cz.mara.nangu.dao.UserRepository;
import cz.mara.nangu.dto.LoginDto;
import cz.mara.nangu.dto.RegisterUserDto;
import cz.mara.nangu.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    @Qualifier(value = "daoAuthProvider")
    private AuthenticationProvider authenticationProvider;

    public void registerUser(RegisterUserDto registerUserDto) {
        if (userRepository.existsByUsername(registerUserDto.getUsername())) {
            throw new IllegalArgumentException("User with given name already exists");
        }
        User user = new User();
        user.setUsername(registerUserDto.getUsername());
        user.setPassword(passwordEncoder.encode(registerUserDto.getPassword()));
        userRepository.save(user);
    }

    public void doLogin(HttpServletRequest req, LoginDto loginDto) {
        //TODO session fixation. Is it necessary or is it managed by spring?
        HttpSession session = req.getSession();
        if (session == null) {
            req.getSession(true);
        } else {
            req.changeSessionId();
        }
        UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(
                loginDto.getUsername(),
                loginDto.getPassword());
        Authentication auth = authenticationProvider.authenticate(authReq);
        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);
    }
}
