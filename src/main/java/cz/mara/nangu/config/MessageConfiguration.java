package cz.mara.nangu.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageConfiguration {

    @Bean
    public Queue naguPostsQueue() {
        return new Queue("nangu.posts");
    }


}
