package cz.mara.nangu.security;

import cz.mara.nangu.dao.UserRepository;
import cz.mara.nangu.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SecurityManager {

    @Autowired
    private UserRepository userRepository;

    public User getCurrentUser() {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User)
                SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> userOpt = userRepository.findByUsername(user.getUsername());
        return userOpt.orElseGet(null);
    }
}
