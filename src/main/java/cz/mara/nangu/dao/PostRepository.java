package cz.mara.nangu.dao;

import cz.mara.nangu.model.Post;
import cz.mara.nangu.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PostRepository extends JpaRepository<Post, Long> {

    boolean existsByIdAndAuthor(Long id, User user);

    Optional<Post> findByIdAndAuthor(Long id, User user);
}
