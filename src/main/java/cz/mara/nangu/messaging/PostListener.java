package cz.mara.nangu.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.mara.nangu.service.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class PostListener {

    private static final Logger log = LoggerFactory.getLogger(PostListener.class);
    @Autowired
    private PostService postService;

    @RabbitListener(queues = "nangu.posts")
    public void receiveMessage(String jsonPost) {
        ObjectMapper objectMapper = new ObjectMapper();
        PostMessage postMessage;
        try {
            postMessage = objectMapper.readValue(jsonPost, PostMessage.class);
        } catch (IOException e) {
            log.error("unable to read message");
            return;
        }
        postService.createPost(postMessage);
    }
}
