package cz.mara.nangu.messaging;

import cz.mara.nangu.model.Post;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * test purposes
 */
@Component
public class MessageProducer {

    @Autowired
    private RabbitTemplate template;
    @Autowired
    private Queue queue;

    @PostConstruct
    public void sendMessage() {
        template.convertAndSend(queue.getName(), "{\"username\": \"mara\", \"text\": \"post from messaging\"}");
    }
}
