package cz.mara.nangu.dto;

import javax.validation.constraints.NotNull;

public class PostUpdateDto {

    @NotNull
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
