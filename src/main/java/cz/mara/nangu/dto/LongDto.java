package cz.mara.nangu.dto;

public class LongDto {

    private Long value;

    public LongDto(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
