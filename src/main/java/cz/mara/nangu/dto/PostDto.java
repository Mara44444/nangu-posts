package cz.mara.nangu.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Valid
public class PostDto {

    @NotBlank
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
