CREATE TABLE nangu_posts."user" (
    id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    username varchar(50) NOT NULL,
    password varchar(200) NOT NULL,
    UNIQUE (username)

);
CREATE TABLE nangu_posts.post (
    id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    text text NOT NULL,
    user_id bigint REFERENCES nangu_posts."user" (id)
)